﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_6_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.6\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_6_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.6\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This library contains VI related to the Domain Management System.

They allow to copy changed files or directory trees on a file server to a corresponding local target to update them before usage.

Main.vi is an example application.

Refer also to: http://wiki.gsi.de/cgi-bin/view/CSframework/DomainManagementSystem

Copyright 2009 GSI

Gesellschaft für Schwerionenforschung mbH
Planckstr. 1, 64291 Darmstadt, Germany
Contact: H.Brand@gsi.de

This file is part of the GenericApplication.

    GenericApplication is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GenericApplication is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GenericApplication.  If not, see &lt;http://www.gnu.org/licenses&gt;.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*5!!!*Q(C=\&gt;5R4BN"&amp;-&lt;RTSB&amp;#AIMCD1JE+`Q4I$E'S"@Y6X",EEK6_F@G=+.;RL,6XB8]"6]B?5`YY&gt;D#9)JAED",G-PX]S__4%\7K2_@*7_;$BX,&amp;Y^XN$@;\2B\;O00[H;_FP0-.4F9&gt;TC7@`C=$%]46K$``48@==3QQPTHUZ`\0],`-V`XZH_-Y=_D]`D9Y`\MZPUX@@M3/V&amp;2%N;U*RG;F-.3:\E3:\E3:\E12\E12\E12\E4O\E4O\E4O\E2G\E2G\E2G\E`318O=B&amp;$CG:0*EI+:I53!:$5@+2?"*0YEE]`+L%EXA34_**0!R2YEE]C3@R*"ZO5_**0)EH]31?3H6*^J-=4_+BP!*0Y!E]A3@Q-+5#4Q!)*AM+"U6A+/A-,A*0Y!E]8#LQ"*\!%XA#$^U+0)%H]!3?Q--N@67C;^J*DI=S=DS/R`%Y(M&gt;$;4E?R_.Y()`D94IZ(M@D)*Q*H?)1Z.TE$("_=4S/BS]Z(M@D?"S0Y['L0S(P+^-U\34(9XA-D_%R0);(%D)]BM@Q'"\$1VE:(M.D?!S0Y7%K'2\$9XA-C$%JU]MI:NRI$$)#Q].0XSX7HV*UC@64KMWLWJ3KT;&lt;;2+L.I8LIKI?J?ECKR6=NKGKR6)OA_O.5;"6'.9HKZD:1?TZXN#VN46P3ZL1:&lt;5K&lt;U-&lt;NVH]]=,`@;\@&lt;;&lt;P&gt;;LV?;\F=;D[@;T;&lt;;4K&gt;;D+:;$Q?(V]$.ZT(&amp;],BP840&gt;7O&lt;[V_DV=06;(6X_8NV^@.W]_X\,@G0J`ZX_F`[(\Q&lt;&gt;;(4OKT2)`6AUC!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.2.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Example" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="calcTargetFilenames.vi" Type="VI" URL="../calcTargetFilenames.vi"/>
		<Item Name="createDir.vi" Type="VI" URL="../createDir.vi"/>
		<Item Name="checkFile.vi" Type="VI" URL="../checkFile.vi"/>
		<Item Name="GPLInfo.vi" Type="VI" URL="../GPLInfo.vi"/>
	</Item>
	<Item Name="copyFile.vi" Type="VI" URL="../copyFile.vi"/>
	<Item Name="copyDirTree.vi" Type="VI" URL="../copyDirTree.vi"/>
</Library>
